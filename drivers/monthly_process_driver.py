"""
File: monthly_process_driver.py
Description: 
"""

import os
import sys
import arrow

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)

from logger import logger
from config import configer

from process.routine_process import RegularAnalyz
from tools.format_time_input import TimeProcess
from tools.get_data_from_csv import CsvData

class MonthlyOutputDriver():

    """驱动月度输出"""

    def __init__(self,date):
        """TODO: to be defined1.

        :param date: TODO

        """
        self._date = date
        self.tmp_file_path = os.path.join(project_root,'data/tmp.trao')

    def process_main(self):
        """Main process
        :returns: TODO

        """
        date_range = TimeProcess.get_month(self._date)
        print(date_range)
        # 初始化过程和周度初始化相同
        origin_data_path = "/Users/wangjunyu/101camp/trao/data/r.csv"
        #start_date = '2019-09-23'
        #end_date = '2019-09-27'

        monthly = RegularAnalyz()
        start,end,diff_days = monthly.param_init(date_range[0],date_range[1])
        j = monthly.fetch_csv(origin_data_path,self.tmp_file_path)
        monthly.change_data_format(j)
        j = monthly.split_days(j,start,end)
        type_list = monthly.collect_types(j)
        r = monthly.data_output(type_list,j,diff_days)
        monthly.jinja_render(r)

    def check_activity_type_driver(self,file_name,type_list):
        file_path = os.path.join(project_root,file_name)
        type_list_ = [type_list]
        data_source = RegularAnalyz()
        j = data_source.fetch_csv(file_path,self.tmp_file_path)
        r = data_source.check_activity_type(j,type_list)
        return r

        

def main():
    # 基于format_time_input.py获取需要处理的时间
    #user_input = "10"
    user_input = "9"
    date_range = TimeProcess.get_month(user_input)
    print(date_range)
    # 初始化过程和周度初始化相同
    origin_data_path = "/Users/wangjunyu/101camp/trao/data/r.csv"
    #start_date = '2019-09-23'
    #end_date = '2019-09-27'
    tmp_file_path = os.path.join(project_root,'data/tmp.trao')

    monthly = RegularAnalyz()
    start,end,diff_days = monthly.param_init(date_range[0],date_range[1])
    j = monthly.fetch_csv(origin_data_path,tmp_file_path)
    monthly.change_data_format(j)
    j = monthly.split_days(j,start,end)
    type_list = monthly.collect_types(j)
    r = monthly.data_output(type_list,j,diff_days)
    monthly.jinja_render(r)

if __name__ == "__main__":
    main()

