"""
File: weekly_process_driver.py
Description: 
"""

import os
import sys
import arrow

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)

from logger import logger
from config import configer

from tools.get_data_from_csv import CsvData
from process.routine_process import RegularAnalyz
from tools.format_time_input import TimeProcess

class WeeklyOutputDriver():
    """
    驱动周度工作日输出，未来如果内容比较多，可以单独生成Driver的文件夹和驱动文件。
    """
    def __init__(self,date,flag,tmp_file_path,input_path):
        self.user_input = date
        self.flag = int(flag)
        self.tmp_file = tmp_file_path
        self.origin_path = input_path

    def process_main(self):
        date_range = TimeProcess.get_week(self.user_input,self.flag)
        logger.info(date_range)
        tmp_file_path = os.path.join(project_root,self.tmp_file)
        origin_data_path = os.path.join(project_root,self.origin_path)
        #"/Users/wangjunyu/101camp/trao/data/r.csv"

        weekly = RegularAnalyz()
        start,end,diff_days = weekly.param_init(date_range[0],date_range[1])
        j = weekly.fetch_csv(origin_data_path,tmp_file_path)
        weekly.change_data_format(j)
        ##logger.debug(j)
        j = weekly.split_days(j,start,end)
        type_list = weekly.collect_types(j)
        r = weekly.data_output(type_list,j,diff_days)
        #print(r)
        print("in main:",type(r))
        r = weekly.add_general_info(start,end,r)
        CsvData.csv_save_dict(r,'/Users/wangjunyu/101camp/trao/aaa.csv','w')
        weekly.jinja_render(r)

def main():
    # 基于format_time_input.py获取需要处理的时间
    user_input = "2019-09-30"
    date_range = TimeProcess.get_week(user_input,0)
    print(date_range)
    
    origin_data_path = "/Users/wangjunyu/101camp/trao/data/r.csv"
    #start_date = '2019-09-23'
    #end_date = '2019-09-27'
    tmp_file_path = os.path.join(project_root,'data/tmp.trao')

    weekly = RegularAnalyz()
    start,end,diff_days = weekly.param_init(date_range[0],date_range[1])
    j = weekly.fetch_csv(origin_data_path,tmp_file_path)
    weekly.change_data_format(j)
    ##logger.debug(j)
    j = weekly.split_days(j,start,end)
    type_list = weekly.collect_types(j)
    r = weekly.data_output(type_list,j,diff_days)
    #print(r)
    print("in main:",type(r))
    r = weekly.add_general_info(start,end,r)
    weekly.jinja_render(r)

if __name__ == "__main__":
    main()
