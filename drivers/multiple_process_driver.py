"""
File: multiple_process_driver.py
Description: 
"""

import os
import sys
import arrow

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)

from logger import logger
from config import configer

from process.routine_process import RegularAnalyz
from tools.format_time_input import TimeProcess
from tools.get_data_from_csv import CsvData

class MultipleProcessDriver():
    '''
    驱动多个周、月的数据处理，并存储在指定的csv文件中
    '''
    def __init__(self,tmp_file_path,input_path,csv_path):
        '''

        '''
        self.tmp_file = tmp_file_path
        self.origin_path = input_path
        # 多周数据处理后的csv文件的存储路径
        self.output_path = csv_path

    def calculate_week_intervals(self,start,end,flag_day,interval):
        '''用于输出多周的数据处理的周期
        start: 起始日期
        end: 结束日期
        flag_day: 从哪天起算
        interval: 计算周期（最大为7）
        interval: 起始和结束的日期间隔
        return: [(s0,e0),(s1,e1)]
        '''
        # 根据输入确定需要处理的周期和周期内的起始时间
        # 不能根据文件自行判断，考虑未来有连续数据之后，也不适合根据文件的起始时间自行判断，还是需要人为指定

        # 处理逻辑
        # 第一步: 获取第一对正确的结果,如果没有正确的结果，结束
        # 第二步: 基于第一对开始的结果，分别计算周期增加后两个值是否为真
        # 如果为真，记录，下一组
        # 如果为假，结束
        r = []
        st = arrow.get(start,tzinfo = 'local')
        et = arrow.get(end,tzinfo = 'local')
        s_info = st.isocalendar()
        logger.debug('s_info: {}'.format(s_info))
        if s_info[2] <= flag_day:
            s = TimeProcess.get_time_by_weeknumber(s_info[1],flag_day)
            e = s.shift(days=interval-1).ceil('day')
            logger.debug('first if s:{},e:{}'.format(s,e))
            logger.debug('st:{},et:{}'.format(st,et))

        elif s_info[2] > flag_day:
            s = TimeProcess.get_time_by_weeknumber(s_info[1]+1,flag_day)
            e = s.shift(days=interval-1).ceil('day')
            logger.debug('second if s:{},e:{}'.format(s,e))
            logger.debug('st:{},et:{}'.format(st,et))
            if e <= et:
                #r.append((s,e))
                logger.debug('yes,we want you!')
        else:
            logger.debug('error')
        flag = True
        while flag:
        # 根据第一组结果计算第二组
            r.append((s,e))
            s = s.shift(days=7)
            e = e.shift(days=7)
            flag = TimeProcess.time_range_judge(st,et,s,e)
        print(r)
        return r
        

         
    def get_range(self,start,end,flag):
        '''
        start_time
        end_time
        flag:
        0 monthly
        1 weekly
        2 week_workday
        return:2d list
        '''
        return {
            '0': 1,
            '1': 2,
            '2': 2,
        }.get(flag, None)


    def process_main(self):
        pass

    def single_process(self,date_range):
        weekly = RegularAnalyz()
        start,end,diff_days = weekly.param_init(date_range[0],date_range[1])
        origin_data_path = os.path.join(project_root,self.origin_path)
        tmp_file_path = os.path.join(project_root,self.tmp_file)
        output_path = os.path.join(project_root,self.output_path)
        j = weekly.fetch_csv(origin_data_path,tmp_file_path)
        weekly.change_data_format(j)
        ##logger.debug(j)
        j = weekly.split_days(j,start,end)
        type_list = weekly.collect_types(j)
        r = weekly.data_output(type_list,j,diff_days)
        #print(r)
        #print("in main:",type(r))
        r = weekly.add_general_info(start,end,r)
        #CsvData.csv_save_dict(r,output_path,'a')
        return r

        # 根据过去的经验正常处理每周周期内的数据

def main():
    t = MultipleProcessDriver('data/tmp.trao','data/r.csv','data/mult_csv_output.csv')
    #t.calculate_week_intervals('2019-10-01','2019-10-31',6,7)
    time_range_list = t.calculate_week_intervals('2019-10-05','2019-10-31',4,7)
    ret = list(map(t.single_process, time_range_list))
    print(ret)
    #[t.single_process(i) for i in time_range_list]
    # Test Case
    #st = arrow.get('2019-10-01',tzinfo='local')
    #et = arrow.now('local')
    #s = arrow.get('2019-10-08',tzinfo='local')
    #e = arrow.get('2019-10-17',tzinfo='local')
    #print(TimeProcess.time_range_judge(st,et,s,e))

if __name__ == "__main__":
    main()
