from config import configer,type_configer
import atl2_api as api
import os

auth = api.get_auth_header()
type_json_list = api.get_types(auth)

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
type_ini_path = os.path.join(project_root,'config/type.ini')

# 简单写法，用for循环

for i in type_json_list:
    name = i["name"]
    if not type_configer.has_section(name):
        type_configer.add_section(name)
        print('有变动')
        print(name)
    for key,value in i.items():
        #configer.set(name,item[0],item[1])

        try:
            if type_configer[name][key] ==  str(value):
            #print('no change')
                pass
            else:
                print('change')
                print(type_configer[name],key,type_configer[name][key],str(value))
                type_configer[name][key] = str(value)
        except:
            pass
            type_configer[name][key] = str(value)
            print('new',type_configer[name][key],value)

type_configer.write(open(type_ini_path,'w'))

#TODO:  问题 无法对guid做唯一性识别，如果有section改名字，不能判断，会单独增加
#TODO:  无法做版本管理
#TODO:  现行处理方案：如果改名，需要全量删除后写入
