"""
File: calendar_heatmap.py
Author: Leon.Junyu
Description: 用于生成日历热力图
"""

# 该功能初步完成, 可以生成html文件
# 需要优化的店参见代码注释TODO部分

import datetime
import arrow
from config import configer,type_configer
import atl2_api as api
# 尝试pyechart 直接生成日历热力图
import pyecharts.options as opts
from pyecharts.charts import Calendar

#考虑针对不同的分类提供不同的热力图输出效果
type_name_you_want = "hf_sport"
start_str_input = "2019-01-01"
end_str_input = "2019-01-31"
#TODO: 上面和下面的start 和 end 需要合并
begin = datetime.date(2019, 1, 1)
end = datetime.date(2019, 1, 31)

def range_to_timestamp(start_str,end_str):
    """用于生成开始和结束日期的时间戳
    输入的格式为：YYYY-MM-DD"""
    #TODO:这个函数是通用功能，考虑放到api去
    start = arrow.get(start_str,'YYYY-MM-DD',tzinfo=tz.tzlocal()).timestamp
    end = arrow.get(end_str,'YYYY-MM-DD',tzinfo=tz.tzlocal()).timestamp
    print(start,end)
    return start,end

#TODO:对于所有的数据处理，初始化的工作是相同的，考虑封装
# 获取授权
auth = api.get_auth_header()
# 获取数据范围
_range = range_to_timestamp(start_str_input,end_str_input)
# 获取数据
j = api.get_certain_intervals(auth,_range[0],_range[1])
data = j['intervals']
# 准备type字典
type_dict = api.get_type_dict()
# 增加变量
data = api.add_parm(data,type_dict)
# 根据需要的分类生成对应的数据列表
#TODO:考虑封装成函数
l = [_i for _i in data if _i["type_name"] == type_name_you_want]
#上面的列表推导式和下面的for循环等价
#l = []
#for _i in data:
#    if _i["type_name"] == type_name_you_want:
#        l.append(_i)


#TODO:热力图生成考虑封装函数
#TODO:增加选项，可以生成图片或者html
data_new = []

for _i in l:
    data_new.append([arrow.get(_i['from_time'],'YYYY-MM-DD HH:mm').
        replace(tzinfo='+08:00').format('YYYY-MM-DD'),_i['time_delta_mins']])

from snapshot_selenium import snapshot as driver
from pyecharts.render import make_snapshot
# 需要安装 snapshot-selenium 或者 snapshot-phantomjs

make_snapshot(driver,(
    Calendar(init_opts=opts.InitOpts(width="1600px", height="1000px"))
    .add(
        series_name="",
        yaxis_data=data_new,
        calendar_opts=opts.CalendarOpts(
            pos_top="120",
            pos_left="30",
            pos_right="30",
            range_="2019",
            yearlabel_opts=opts.LabelOpts(is_show=False),
        ),
    )
    .set_global_opts(
        title_opts=opts.TitleOpts(pos_top="30", pos_left="center", title="2019年步数情况"),
        visualmap_opts=opts.VisualMapOpts(
            max_=20000, min_=500, orient="horizontal", is_piecewise=False
        ),
    )
    .render("calendar_heatmap.html")
), "Calendar.png")

#TODO: 调整生成图片和html的位置


#TODO: 增加main的内容

#make_snapshot(driver,Calendar().render("calendar_heatmap.html"), "Calendar.png")
