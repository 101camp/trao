from requests.auth import HTTPBasicAuth
import requests
import json
from datetime import datetime
import arrow

import platform
import pyperclip


from config import configer,type_configer
from logger import logger

INTERVAL_MAX = configer.get('tr_related','interval_max')
logger.debug(INTERVAL_MAX)

def write_to_clipboard(output):
    if (platform.system() == 'Darwin') :
        import subprocess
        process = subprocess.Popen('pbcopy', env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
        process.communicate(output.encode())
    elif (platform.system() == 'Windows') :        
        pyperclip.copy(output)
    elif (platform.system == 'Linux') :
        logger.info('linux system has not been completed')
    else :
        pass
    

def get_type_dict():
    """通过type.ini文件生成type的字典
    :returns: {guid:type_name}

    """
    type_dict = {}
    for each_section in type_configer.sections():
        type_dict[type_configer.get(each_section,'guid')] = type_configer.get(each_section,'name')
    #print(type_dict)
    return type_dict

def time_delta_transfer(time_delta):
    """将分钟数据转换为小时分钟显示
    :param time_delta: 时间差数据，int型（小数转为int）
    :returns: 20h3m str
    """
    time_delta = int(time_delta)
    hours = int(time_delta/60)
    mins = time_delta - hours*60
    return str(hours) + "h" + str(mins) + "m"

def add_parm(list_data,type_dict):
    """在源数据中增加用于辅助展示的字典键值对

    :param list_data: TODO
    :returns: TODO

    """
    def time_delta_compute(dict_data):
        time_delta = int((dict_data['to'] - dict_data['from'])/60)
        dict_data['time_delta_mins'] = time_delta
        return dict_data
    for _j in list_data:
        _j['from_time'] = arrow.get(_j['from']).to('+0800').format('YYYY-MM-DD HH:mm')
        _j['to_time'] = arrow.get(_j['to']).to('+0800').format('YYYY-MM-DD HH:mm')
        _j['type_name'] = type_dict[_j['type']['guid']]
        time_delta_compute(_j)
    return list_data

def get_certain_data(data,kw):
    """
    根据指定的类型guid 和 数据，获取对应的清单
    """
    #TODO: 写的太丑了，虽然功能能实现，但是不够pythonic
    #TODO: 传参还是没搞明白
    #TODO: 这个函数不一定需要，根据后面写的结果考虑去掉
    r = []
    _tmp_type_list = []
    for _k,_v in kw.items():
        _tmp_type_list.append(_v)
    #TODO from穗佳 list.extend, 生成器


    #TODO from穗佳 list.extend, 生成器，生成式，看文敏的代码
    for x in data['intervals']:
        if x['type']['guid'] in _tmp_type_list:
            print(x)
            r.append(x)
        else:
            pass
    return r

def get_centain_types(*args):
    """
    获取指定的类型guid
    """
    r = {}
    #print(args)
    #TODO: 这里的传参非常别扭，后面要改,传参没弄明白
    for _i in args[0]:
        print(_i)
        r[_i] = type_configer.get(_i,'guid')
    return r

def get_auth_header():
    """
    Generate basic auth header for aTimeLogger.
    See the post for more info about the API, http://blog.timetrack.io/rest-api/
    """

    # get username and password from text file
    #lines = [line.rstrip('\n') for line in open('pass.txt')]
    username = configer.get('atl2_account_info', 'username')
    password = configer.get('atl2_account_info', 'password') 
    return HTTPBasicAuth(username, password)

def get_types(auth_header):
    """
    Retrieve types data from aTimeLogger.
    :param auth_header: auth header for request data.
    :return: A list of dict for types data.
    """
    r_type = requests.get("https://app.atimelogger.com/api/v2/types",
                          auth=auth_header)
    types = json.loads(r_type.text)
    return types['types']


def get_certain_intervals(auth_header,start_date,end_date):
#def get_certain_intervals(auth_header,*date_range):
    """
    Retrieve all intervals data from aTimeLogger. Number of entries is limited by INTERVAL_MAX.
    :param auth_header: auth header for request data.
    :return: A list of dict for intervals data.
    """
    #print(*date_range)
    r_interval = requests.get("https://app.atimelogger.com/api/v2/intervals",
                              #params={'limit': INTERVAL_MAX, 'order': 'asc'},
                              params={'from': start_date, 'to':end_date,
                                  'order': 'dsc','limit': INTERVAL_MAX,},
                              auth=auth_header)
    intervals = json.loads(r_interval.text)
    #return intervals['intervals']
    #TODO: 关于比较还需要再调整一下,现在的这个值是延续TimeReport项目的最大值
    if intervals['meta']['total'] < intervals['meta']['limit']:
        print(intervals['meta'])
        return intervals
    elif intervals['meta']['total'] > intervals['meta']['limit']:
        print('total is more')


def get_all_intervals(auth_header):
    """
    #TODO: 暂时弃用该函数
    Retrieve all intervals data from aTimeLogger. Number of entries is limited by INTERVAL_MAX.
    :param auth_header: auth header for request data.
    :return: A list of dict for intervals data.
    """
    r_interval = requests.get("https://app.atimelogger.com/api/v2/intervals",
                              #params={'limit': INTERVAL_MAX, 'order': 'asc'},
                              params={'limit': '100', 'order': 'dsc'},
                              auth=auth_header)
    intervals = json.loads(r_interval.text)
    return intervals['intervals']


def get_new_intervals(auth_header):
    """
    #TODO: 这个函数被弃用了暂时，还没搞清楚接口的用法
    Retrieve new intervals data from aTimeLogger. New intervals is defined by DAYS_NEW.
    :param auth_header: auth header for request data.
    :return: A list of dict for intervals data.
    """
    now = arrow.get(datetime.now(), 'US/Eastern')
    #print(now)
    print(now.timestamp)
    #start = now.replace(day=-2).floor('day')
    #start = now.shift(day=-DAYS_NEW).floor('day')
    start = now.floor('day')
    print(start.timestamp)
    #start = now.replace(hours=5).floor('day')
    r_interval = requests.get("https://app.atimelogger.com/api/v2/intervals",
                              params={'from': str(start.timestamp), 'to': str(now.timestamp)},
                              auth=auth_header)
    intervals = json.loads(r_interval.text)
    print(intervals['intervals'])

    return intervals['intervals']

#a = get_auth_header()
#print(get_types(a))
#print(get_new_intervals(a))
#print(get_all_intervals(a))
