import sys
print(sys.path)
#print(sys.__file__)

import re
print(re.__file__)

def myFun():
    print("the value of __name__ in a.py is: ",__name__)

def main():
    myFun()

if __name__ == "__main__":
    print('main in a.py activate')
    main()
