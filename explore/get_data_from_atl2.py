"""
File: data_source.py
Author: John Yul
Description: 从atlapi获取数据源,初步处理后存储到本地文件
"""

import os
import sys
from requests.auth import HTTPBasicAuth
import requests
import json
import csv

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
config_file_path = os.path.join(project_root, 'config/config.ini')
sys.path.append(project_root)
from src.logger import logger
from src.config import configer,type_configer
#TODO: 以上两行未来可能要调整，目前是oop和非oop共存的情况

name=os.path.join(project_root,'data/csv/report-24-09-2019.csv')

class data_source_atl2():
    def __init__(self):
        INTERVAL_MAX = configer.get('tr_related','interval_max')
        self.INTERVAL_MAX = INTERVAL_MAX

    def get_config(self):
        _data_source = configer.get('general','data_source')
        username = configer.get('atl2_account_info', 'username')
        password = configer.get('atl2_account_info', 'password') 
        logger.info(_data_source)

    def get_auth_header(self):
        """
        Generate basic auth header for aTimeLogger.
        See the post for more info about the API, http://blog.timetrack.io/rest-api/
        """
    
        # get username and password from text file
        #lines = [line.rstrip('\n') for line in open('pass.txt')]
        username = configer.get('atl2_account_info', 'username')
        password = configer.get('atl2_account_info', 'password') 
        return HTTPBasicAuth(username, password)

    def get_atlapi_data(self,auth_header,start_date,end_date):
    #def get_certain_intervals(auth_header,*date_range):
        """
        Retrieve all intervals data from aTimeLogger. Number of entries is limited by INTERVAL_MAX.
        :param auth_header: auth header for request data.
        :return: A list of dict for intervals data.
        """
        #print(*date_range)
        r_interval = requests.get("https://app.atimelogger.com/api/v2/intervals",
                                  #params={'limit': INTERVAL_MAX, 'order': 'asc'},
                                  params={'from': start_date, 'to':end_date,
                                      'order': 'dsc','limit': self.INTERVAL_MAX,},
                                  auth=auth_header)
        print(r_interval)
        intervals = json.loads(r_interval.text)
        #return intervals['intervals']
        #TODO: 关于比较还需要再调整一下,现在的这个值是延续TimeReport项目的最大值
        if intervals['meta']['total'] < intervals['meta']['limit']:
            print(intervals['meta'])
            return intervals
        elif intervals['meta']['total'] > intervals['meta']['limit']:
            print('total is more')

if __name__ == "__main__":
    data = data_source_atl2()
    data.get_config()
    data.get_auth_header()
    import arrow
    start_time = arrow.get('2019-09-20').timestamp
    logger.info(start_time)
    end_time = arrow.get('2019-09-23').timestamp
    logger.info(end_time)

