"""
File: weekly_new.py
Author: 
Description: 
"""
import pyperclip
import arrow
from config import configer,type_configer
import atl2_api as api
#import weekly as wl
from logger import logger
import os


project_root = os.path.dirname(os.path.dirname(
    os.path.realpath(__file__)))
#weekly_jinja_template = configer.get('path_config','weekly_jinja_template')
weekly_jinja_template = configer.get('path_config','weekly_jinja_template')
#weekly_save_name = configer.get('path_config','weekly_save_name')
weekly_save_name = os.path.join(project_root, configer.get('path_config','weekly_save_name'))

# 采用总值、平均值、次数计算方案的分类
l = []
for each_section in type_configer.sections():
    if type_configer.get(each_section,'group') == 'False':
        l.append(type_configer.get(each_section,'name'))
#l = ['sleep','noonsleep','life_family','hf_sport','life_work','output_career','o_others','live_dinner','live_affair','live_traffic','p_Office','p_Home']
logger.debug(l)


def get_workday_range(input_date):
    def get_workdays(input_date):
        start_date = input_date.shift(days=-input_date.isocalendar()[2]+1).floor('day').timestamp
        end_date = input_date.shift(days=5-input_date.isocalendar()[2]).ceil('day').timestamp
        #return {'from':start_date,'to':end_date}
        return start_date,end_date

    flag_num = input_date.isocalendar()[2]
    if flag_num in range(1,6):
        #print(get_workdays(input_date.shift(days=-7)))
        r = get_workdays(input_date.shift(days=-7))
        return r
    elif flag_num in range(6,8):
        #print(get_workdays(input_date))
        r = get_workdays(input_date)
        return r
    else:
        print('wrong')
        exit(0)



# 预处理阶段
# 获取授权
auth = api.get_auth_header()
# 获取数据范围
workday_range = get_workday_range(arrow.now().to('+0800'))
# 获取数据
j = api.get_certain_intervals(auth,workday_range[0],workday_range[1])
#number_of_week = 35
monday_of_week = arrow.get(workday_range[0]).to('+0800').format('YYYY-MM-DD')
friday_of_week = arrow.get(workday_range[1]).to('+0800').format('YYYY-MM-DD')
number_of_week = arrow.get(workday_range[0]).to('+0800').isocalendar()[1]

data = j['intervals']
# 准备type字典
type_dict = api.get_type_dict()
# 增加变量
data = api.add_parm(data,type_dict)
logger.debug(data)


#TODO：这是之前没想到的# 对期初、期末的超期数据的处理


def output_data_unit(data,type_guid,type_name):
    _tmp_l = []
    all_m = 0
    times = 0
    for _i in data:
        if _i['type']['guid'] == type_guid:
            all_m += _i['time_delta_mins']
            times += 1
            _tmp_l.append({'type_name':_i['type_name'],'from_time':_i['from_time'],
                'to_time':_i['to_time'],'time_delta_mins':_i['time_delta_mins'],
                'comment':_i['comment']})
    #__tmp_l = []
    #for _i in _tmp_l:
    #    __tmp_l.append(str(_i)+'\n')
    if times == 0:
        everage = 0
    else:
        everage = all_m/times
    #logger.debug(type_name,all_m,times,everage,_tmp_l)
    #return _tmp_l
    #return all_m,times,everage
    return {'type_name':type_name,'all_m':all_m,'times':times,'everage':everage,
            'detail_list':_tmp_l}

def output_all(data,type_list,type_dict):
    """输出需要的总值、平均和次数以及明细

    :param data: TODO
    :param type_list: TODO
    :param type_dict: TODO
    :returns: TODO

    """
    _tmp_r = []
    for _item in l:
        for guid,name in type_dict.items():
            if name == _item:
                logger.debug(guid)
                out_guid = guid
                _tmp_r.append(output_data_unit(data,out_guid,_item))
    #return _tmp_r
    r = {}
    for _item in _tmp_r:
        for _key,_value in _item.items():
            logger.debug(_key)
            new_key = _item['type_name'] + '_' +  _key
            r[new_key] = _value
    logger.info(r)
    return r

r = output_all(data,l,type_dict)
logger.debug(r)
r['monday_of_week'] = monday_of_week
r['friday_of_week'] = friday_of_week
r['number_of_week'] = number_of_week

from jinja2 import Environment,FileSystemLoader,PackageLoader, select_autoescape
from jinja2 import Template

#env = Environment(loader=FileSystemLoader('.'))
#env = Environment(loader=FileSystemLoader('..'))
env = Environment(loader=FileSystemLoader(project_root))
logger.info(env)
env.globals['time_delta_transfer'] = api.time_delta_transfer

#template = env.get_template('data/templates/my_wk.j2')
template = env.get_template(weekly_jinja_template)
content = template.render(r)

with open(weekly_save_name, "w") as fh:
    fh.write(content)

api.write_to_clipboard(content)

