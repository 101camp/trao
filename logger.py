#TODO: 增加 logger.py 的内容，把日志输出管理起来
import os
import datetime
import logging.config
#from logging.handlers import MemoryHandler
#import logging


# 当level等于FLUSH_LEVEL的时候，输出结果
#FLUSH_LEVEL = 42
#logging.addLevelName(FLUSH_LEVEL, 'FLUSH')

# 缓冲区容量
#BUFFER_SIZE = 256

_project_root = os.path.dirname(os.path.realpath(__file__))
_current_dir = os.path.dirname(os.path.realpath(__file__))

log_config_path = os.path.join(_project_root, 'configs/logging_config.ini')

logging.config.fileConfig(log_config_path)
#logger = logging.getLogger('root')
logger = logging.getLogger('trao')
a = logging.getLogger()
#__import__('ipdb').set_trace()

#logger.debug(logger.handlers)

stream_handler = logger.handlers[0]
#logger.debug(type(stream_handler))

log_path = os.path.join(_project_root, 'data/log/')
os.makedirs(log_path, exist_ok=True)

# 同时输出log到文件中
filename = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S') + '.log'
file_handler = logging.FileHandler(os.path.join(log_path, filename), mode='w', encoding='utf-8')
file_handler.setLevel(stream_handler.level)
file_handler.setFormatter(stream_handler.formatter)
#file_handler = logger.handlers[1]
logger.addHandler(file_handler)

