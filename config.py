#!/usr/bin/env python3
# coding=utf-8

'''
Created on 2019-08-30

@author: John Yul
'''
import os
import configparser


project_root = os.path.dirname(os.path.realpath(__file__))


"""
    configer
"""
configer = configparser.ConfigParser()
current_dir = os.path.dirname(os.path.realpath(__file__))
configer.read(os.path.join(project_root, 'configs/config.ini'), encoding="utf-8")

type_configer = configparser.ConfigParser()
type_configer.read(os.path.join(project_root, 'configs/type.ini'), encoding="utf-8")


#if not configer.has_section('date'):
#    configer.add_section('date')
# 
#if not configer.has_option('date', 'xxx'):
#    configer.set('date', 'yyy', 'zzz')
