"""
File: weekly_csv.py
Author: 
Description: 
"""
import pyperclip
import arrow
import os
import sys
import platform
import pyperclip
from dateutil import tz

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)

from logger import logger
from config import configer

from tools.get_data_from_csv import CsvData
from tools.format_time_input import TimeProcess
from tools.jinja_enhance import JinjaEnhance

class RegularAnalyz():
    def __init__(self):
        pass

    def fetch_csv(self,origin_data_path,tmp_file_path):
        """
        获取csv数据
        """
        tmp_list = CsvData.get_csv_data(origin_data_path)
        final_list = CsvData.csv_file_check(tmp_list)
        CsvData.csv_save(final_list,tmp_file_path)
        j = CsvData.get_csv_data(tmp_file_path)
        return j

    def param_init(self,start_date,end_date):
        """
        start_date
        end_date
        week_number
        """
        #start = arrow.get(start_date).replace(tzinfo='+08:00')
        #end = arrow.get(end_date).replace(tzinfo='+08:00').shift(hours=+23,minutes=+60,seconds=-1)
        diff_days = (end_date - start_date).days + 1
        logger.debug("start: {}".format(start_date))
        logger.debug("end: {}".format(end_date))
        #logger.debug(start,end,diff_days)
        return start_date,end_date,diff_days

    def change_data_format(self,j):
        """
        调整数据格式
        增加必要的列
        """
        # 数据格式调整
        for _i in j[1:]:
            #arrow.get(_i[2],"YYYY-MM-DD HH:mm:ss")
            _i[2] = arrow.get(_i[2],tzinfo=tz.tzlocal())
            _i[3] = arrow.get(_i[3],tzinfo=tz.tzlocal())
            _i.append(_i[3]-_i[2])
        #logger.debug(j)
        return j

    def split_days(self,j,start,end):
        """
        将跨过12点的数据做调整
        输入：开始、结束日期、数据
        输出：分割好的数据
        """
        final = []
        wait = []
        
        for _i in j[1:]:
            if (start < _i[2] < end) and (start<_i[3]<end):
                final.append(_i)
                #print("normal",_i)
            elif (_i[3]<start) or (_i[2]>end):
                pass
                #print("out",_i)
            elif _i[2]<start and start<_i[3]<end:
                s_cut = _i
                s_cut_1 = s_cut.copy()
                s_cut_1[3] = start.shift(seconds=-1)
                s_cut_1[4] = s_cut_1[4]+" cut out"
                # 周期内的起始数据
                s_cut_2 = s_cut.copy()
                s_cut_2[2] = start
                s_cut_2[4] = s_cut_2[4] + " cut in"

            elif _i[3]>end and start<_i[2]<end:
                e_cut = _i
                e_cut_1 = e_cut.copy()
                e_cut_1[3] = end
                e_cut_1[4] = e_cut_1[4] + " cut in"

                e_cut_2 = e_cut.copy()
                e_cut_2[2] = end.shift(seconds=+1)
                e_cut_2[4] = e_cut_2[4] + " cut out"
        
        #final.extend([s_cut_2,e_cut_1])
        logger.debug(final)
        return final

    def collect_types(self,j):
        """
        根据数据中的类别生成列表列表
        """
        tmp = [_i[0] for _i in j[1:]]
        r = list(set(tmp))
        return r

    def data_unit(self,data_details,type_name,days):
        """
        对某一个指定类别的数据的汇总
        """
        times = 0
        detail_list = []
        all_m = arrow.now() - arrow.now()
        for i in data_details:
            i[2] = i[2].format("YYYY-MM-DD HH:mm:ss ZZ")
            i[3] = i[3].format("YYYY-MM-DD HH:mm:ss ZZ")
            if i[0] == type_name:
                times += 1
                #i[2] = i[2].format("YYYY-MM-DD HH:MM:SS")
                #i[3] = i[3].format("YYYY-MM-DD HH:MM:SS")
                detail_list.append(i)
                all_m += i[5]
        everage = all_m / days
        #__import__('ipdb').set_trace()
        # 对最终展示的形态做转换
        all_m_h,all_m = self.time_delta_format(all_m)
        everage_h,everage = self.time_delta_format(everage)
        r = {"type_name":type_name,
                "all_m":all_m,"all_m_h":all_m_h,"times":times,
                "everage":everage,"everage_h":everage_h,
                "detail_list":detail_list}
        return r

    def time_delta_format(self,time_delta):
        seconds_int = int(time_delta.total_seconds())
        m_all, s = divmod(seconds_int, 60)
        h, m = divmod(m_all, 60)
        return '{:d}:{:02d}'.format(h, m), str(m_all)+"mins"

    def data_output(self,type_l,final,days):
        r = {}
        for e in type_l:
            tmp_r = self.data_unit(final,e,days)
            for k,v in tmp_r.items():
                new_key = e+"_"+k
                r[new_key] = v
        return r

    def jinja_render(self,r):
        weekly_jinja_template = configer.get('path_config','weekly_jinja_template')
        weekly_save_name = os.path.join(project_root, configer.get('path_config','weekly_save_name'))
        from jinja2 import Environment,FileSystemLoader,PackageLoader, select_autoescape
        from jinja2 import Template
        env = Environment(loader=FileSystemLoader(project_root))
        logger.info(env)
        #env.globals['combine_data'] = combine_data
        env.globals['combine_data'] = JinjaEnhance.combine_data
        template = env.get_template(weekly_jinja_template)
        content = template.render(r)

        with open(weekly_save_name, "w") as fh:
            fh.write(content)



    def write_to_clipboard(self,output):
        if (platform.system() == 'Darwin') :
            import clipboard
            clipboard.copy(str(output))  # now the clipboard content will be string "abc"
            text = clipboard.paste()  # text will have the content of clipboard
        elif (platform.system() == 'Windows') :        
            pyperclip.copy(output)
        elif (platform.system == 'Linux') :
            logger.info('linux system has not been completed')
        else :
            pass

    def add_general_info(self,start_date,end_date,r):
        """
        用于最后增加通用的数据
        """
        #number_of_week = arrow.get(start_date).replace(tzinfo='+08:00').isocalendar()[1]
        number_of_week = end_date.isocalendar()[1]
        #print(type(r))
        r["start_date"] = start_date.strftime('%Y-%m-%d')
        r['end_date'] = end_date.strftime('%Y-%m-%d')
        r['number_of_week'] = number_of_week
        return r

    @staticmethod
    def check_activity_type(data_list,type_list):
        """
        用于检查数据中是否存在或者不存在相关类型的数据
        data_list: 二维数组，需要检查的数据
        type_list: 需要检查的类别的名称列表
        return:
            如果存在，返回对应的数据
            如果不存在，返回空
        """
        #TODO: 考虑性能问题，如果data_list特别大，可能需要考虑用type_list 在data_list 中迭代检查
        r = [_i for _i in data_list if _i[0] in type_list]
        return r





def main():
    # 基于format_time_input.py获取需要处理的时间
    user_input = "2019-09-30"
    date_range = TimeProcess.get_week(user_input,0)
    print(date_range)
    
    origin_data_path = "/Users/wangjunyu/101camp/trao/data/r.csv"
    #start_date = '2019-09-23'
    #end_date = '2019-09-27'
    tmp_file_path = os.path.join(project_root,'data/tmp.trao')

    weekly = RegularAnalyz()
    start,end,diff_days = weekly.param_init(date_range[0],date_range[1])
    j = weekly.fetch_csv(origin_data_path,tmp_file_path)
    weekly.change_data_format(j)
    ##logger.debug(j)
    j = weekly.split_days(j,start,end)
    type_list = weekly.collect_types(j)
    r = weekly.data_output(type_list,j,diff_days)
    #print(r)
    print("in main:",type(r))
    r = weekly.add_general_info(start,end,r)
    weekly.jinja_render(r)
    #weekly.write_to_clipboard(r)

if __name__ == "__main__":
    main()
