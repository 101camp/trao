#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
File: driver.py
Author: John Yul
Email: trao@johnyul.com
Github: https://gitlab.com/101camp/trao
Description: 程序主入口
"""
import argparse

from tools.get_data_from_csv import CsvData as csv
from drivers.weekly_process_driver import WeeklyOutputDriver
from drivers.monthly_process_driver import MonthlyOutputDriver



def main():

    example_text = '''example:

    python driver.py -mr 10
    python driver.py -wr 2019-10-07 0 #周一至周五
    python driver.py -wr 2019-10-07 1 #上周六日&本周一至周五
    python driver.py -tc data/r.csv begin
    '''

    parser = argparse.ArgumentParser(epilog=example_text,formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-ip", "--input_path", type=str, default="data/r.csv", help="dir path to get data")

    parser.add_argument("-tp", "--tmp_file_path", type=str, default="data/tmp.trao", help="tmp file path")

    parser.add_argument('-cp', '--csv_path', type=str,
            default="data/mult_csv_output.csv")

    #输出结果的路径暂时存放在config.ini文件中，和jinja的配置在一起
    #parser.add_argument("-op", "--output_path", type=str, default="",
    #                    help="dir path to store result")

    parser.add_argument("-wr", "--weekly_routine", type=str, nargs=2,
            help=''' Output weekly result
           0:取周一至周五，1：取上周日至本周五
            ''')
    parser.add_argument("-mr", "--monthly_routine", type=str, 
            help="Output monthly result")
    parser.add_argument('-tc', '--type_check',type=str, nargs=2,
            help=""" Check cetrain type activity exists or not.
            arg1: filename
            arg2: typename
            """)



    # 写条件
    args = parser.parse_args()

    # 处理条件选择
    if args.weekly_routine:
        print("args.weekly_routine:",args.weekly_routine)
        print("run weekly_routine")
        weekly_ = WeeklyOutputDriver(args.weekly_routine[0],
                args.weekly_routine[1],args.tmp_file_path,args.input_path)
        weekly_.process_main()
    elif args.monthly_routine:
        print("args.monthly_routine:",args.monthly_routine)
        print("run monthly_routine")
        monthly_ = MonthlyOutputDriver(args.monthly_routine)
        monthly_.process_main()

    elif args.type_check:
        #TODO: 因为方法在该类中，类初始化必须赋值，该值没有意义，未来去掉
        data = MonthlyOutputDriver('9')
        r = data.check_activity_type_driver(args.type_check[0],args.type_check[1])
        print(r)

    else:
        print("WIP")

    # 文件输出选择

    # 临时测试用，必要时删除


if __name__ == "__main__":
    main()
