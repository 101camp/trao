# ChangeLog

## Release

参见 [release.md](docs/release.md)

## Slide

参见[幻灯和汇报说明](docs/slides)目录下的内容。

## Event

*初期准备阶段* 

 * 2019-09-16 - 2019-09-24 对项目的基本功能和文档做调整，参见前14个Issue（#1 - #14 ）
 * 2019-09-24 trao-v0.1.1 版本发布，具体信息见[release文档](docs/release.md)
 * 2019-09-23 晚，基于大妈的提醒，调整湍道的中文说明
 * 2019-09-22 [101camp3py分享](docs/slides/01w_101camp_3py_report.md)
 * 2019-09-21 [怼圈分享](docs/slides/01w_du127w_w38_report.md)
 * 2019-09-29 启用项目wiki
 * 2019-09-30 trao-v0.1.2 版本发布，具体信息见[release文档](docs/release.md)
 * 2019-10-04 trao-v0.1.3 版本发布，具体信息见[release文档](docs/release.md)
 * 2019-10-11 第一位开发者加入，组建 `TraoDevTeam`，开始准备团队协同推动项目发展
 * 2019-10-13 准备开始项目的运营工作 #45
 * 2019-10-29 trao-v0.1.4 版本发布，具体信息见[release文档](docs/release.md)
 
*项目正式开源*
 
 * 2019-09-16 迁移到独立的独立[仓库](https://gitlab.com/101camp/trao)，获取中文名
    * 项目完成迁移，做好了之前的tag分支的备份迁移工作。之前的记录在下面的链接中依旧可见。
    * 在[Files · Wangjunyu · 101Camp / 2py / tasks · GitLab](https://gitlab.com/101camp/2py/tasks/tree/Wangjunyu)中作为大作业提交后继续开发。
大妈[反馈](https://gitlab.com/101camp/2py/tasks/commit/7580fad68a90a0f4606a2eec8edd97a13cc01839#note_217334667)，遂成为了独立的项目。
    * [[docs]complete related docs about this branch's dev (9ee450ab) · Commits · 101Camp / 2py / tasks · GitLab](https://gitlab.com/101camp/2py/tasks/commit/9ee450ab1b8907d7db400984dc4afd9e9e100114#note_217330414)


*课程仓库阶段*

* trao-v0.1.0 版本发布，具体信息见[release文档](docs/release.md)
* 2019-09-15 对文档整体做调整，符合开源项目的基本要求
* 2019-09-09 确定项目名称为trao
* 2019-09-08 更新Readme
* 2019-09-01 丰富内容,增加具体用法
* 2019-08-30 新建
