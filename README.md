<p align="center">
  <img src="https://pic.junyuio.com/img/trao/logo_2019-09-21.png?x-oss-process=image/resize,h_100" alt="trao" />
</p>

# trao

时账分析展示器(time record analysis output);

又名: 湍(tuān)道 TRAO 项目

时间如湍流不息之河,每个人的道路包含其中; 湍道是帮助我们自动化探寻并可视化展现时间之道的工具集;

## 准备工作

*你是时间记录者么？*

* 如果你使用[aTimeLogger 2](http://www.atimelogger.com/)做时间记录，目前的trao-v0.1.4版本利用atl2的csv导出时间报告做初步的分析展示。
* 如果你使用[Time Meter Time Sheet](http://timemeter.io/)做时间记录，请稍等，我们正在开发相关功能。
* 如果你使用其他工具记录时间，只要能导出为csv格式的内容,也可以进行分析和输出。目前功能还在继续优化，我们需要你的支持，
* 如果你并没有时间记录，只是感兴趣，想了解一下，可以从下方扫描二维码或者微信公众号搜索“湍道”或者“Trao”了解更多。

*你希望了解、分析、展示自己的行为和花费的时间么？*

* atl2用户: 在开始使用trao-v0.1.4的功能前，你需要做一些基础的配置工作，参见[这里](docs/deploy.md)。
* 其他用户: 如果你用其他的方式做时间记录，欢迎您加入到我们的开源项目的开发或者其他工作中，一起来完善相关功能。

## 当前功能展示

目前已经实现的功能如下:
* 在配置完成后，在项目的根目录下执行对应的命令即可获取对应的结果。
  * 注意：命令行有几个可选的命令选项，输入`python driver.py -h`可以看到

```
usage: driver.py [-h] [-ip INPUT_PATH] [-tp TMP_FILE_PATH] [-cp CSV_PATH]
                 [-wr WEEKLY_ROUTINE WEEKLY_ROUTINE] [-mr MONTHLY_ROUTINE]
                 [-tc TYPE_CHECK TYPE_CHECK]

optional arguments:
  -h, --help            show this help message and exit
  -ip INPUT_PATH, --input_path INPUT_PATH
                        dir path to get data
  -tp TMP_FILE_PATH, --tmp_file_path TMP_FILE_PATH
                        tmp file path
  -cp CSV_PATH, --csv_path CSV_PATH
  -wr WEEKLY_ROUTINE WEEKLY_ROUTINE, --weekly_routine WEEKLY_ROUTINE WEEKLY_ROUTINE
                        Output weekly result 0:取周一至周五，1：取上周日至本周五
  -mr MONTHLY_ROUTINE, --monthly_routine MONTHLY_ROUTINE
                        Output monthly result
  -tc TYPE_CHECK TYPE_CHECK, --type_check TYPE_CHECK TYPE_CHECK
                        Check cetrain type activity exists or not. arg1:
                        filename arg2: typename

example:

    python driver.py -mr 10
    python driver.py -wr 2019-10-07 0 #周一至周五
    python driver.py -wr 2019-10-07 1 #上周六日&本周一至周五
    python driver.py -tc data/r.csv begin
```

以`python driver.py -i data/r.csv -o data/result.md -wd 2019-09-20 2019-10-04`为例，`-i 表示输入的文件`,`-o 表示输出的文件`,`-wd 表示采用用户输入的起始日期进行分析，日期的格式为YYYY-MM-DD`。

* 以我的模板为例，生成的内容可以直接放到编辑器中渲染成为文章模板

* 将文章模板发布后，文章的内容参考下方样例:
[文章样例](https://mp.weixin.qq.com/s/MbUNSW49m_1bLos9o7we0g)

## 功能设计

该项目期望实现的功能逻辑图如下所示，图中的数字对应图片下方的功能说明(关于需求的具体说明参见[todo.md](docs/todo.md)):

![](https://pic.junyuio.com/img/trao/trao-function-v0.1.jpg)

1. atl2的api数据调用（已初步实现,atl2接口发生变更，该功能需要升级）
2. 对csv格式的数据读取 (已初步实现)
3. 数据清理：根据默认规则对数据做处理
   * 对跨日的数据做自动截断(已初步实现)
   * 根据自己设置的规则对数据做检查和错误提示（比如未分类数据等）
4. 数据存储
   * 将清理完的数据存储在本地对应的目录下
   * 未来考虑存入本地数据库
   * 如果发布共有服务，则提供公共的数据存储服务
5. 数据加工
   * 根据用户的规则做数据的加工，主要的规则分为常规检查和专项检查
   * 常规检查包括：日、周、月、季度、年度的数据报告的生成
   * 专项检查包括：根据一定时间周期类的分类或者备注信息的数据报告生成
6. API调用
   * 后端功能通过rest api 封装，未来和前端通信
7. 前端展示
   * 通过前端能更加方便的进行时间分析处理
8. 其他用途
   * 比如生成自己需要的文章草稿
   * 制作基于 mac 下 alfred 的workflow的自动化分析工具等

上述的功能目前只实现了最小的MVP，剩余的功能需要大家一起来开发。

## 项目进展

项目还在抓紧开发中。最新的发布为tag为trao-v0.1.4的版本。
新版本即将发布。

功能发布参考[release 文档](docs/release.md)

如果想了解项目的进展或者其他材料，可以查看[Wiki](https://gitlab.com/101camp/trao/wikis/home)


## 特别之处

该项目初心, 参见[About](https://gitlab.com/101camp/trao/wikis/About)

* 这是一个对新手友好的项目。如果你觉得哪里的文档不够清楚，请发Issue 告诉我们。
* 这里欢迎大家提出自己的想法，实践开源项目理念。

## 参与项目

欢迎你参与项目，不仅仅是开发，只要你有兴趣，这里总有一些你可以参与的事情。具体的内容见[如何参与](CONTRIBUTING.md)。


未尽事宜请联系 @Wangjunyu 沟通


| 湍道微信公众号                                                     | 项目发起人微信                                                     |
| -------------------------------------------------------- | ---------------------------------------------------------- |
| <img src="https://pic.junyuio.com/img/trao/qrcode_for_wechat_public_account_trao_8cm.jpg" height="150" width="150"> | <img src="https://pic.junyuio.com/img/personal-info/WechatAccout-2019-09-15.jpeg" height="150" width="150"> |

