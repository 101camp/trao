name: [release]计划发布trao-vx.x.x

# 发布计划

## 背景

计划在近期发布`trao-vx.x.x`版本，主要考虑如下：
* 
* 

## 功能
此次预计的内容更新主要有：

* #

## 检查

- [ ] 确认功能完备可执行
- [ ] 更新readme
- [ ] 部署文档
- [ ] release 文档 
- [ ] 其他文档

- [ ] Changelog文档(发布当天修改）

## 方式

* 从master分支新建release分支 release/vx.x.x
* 将develop和其他的分支合并到release分支
* 在release分支做好测试
   * 文档链接测试
   * 核心功能测试
* 合并到master分支
* 打标签 trao-vx.x.x
* 删除release分支
