import os
import sys


project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)

from logger import logger
from config import configer

from process.routine_process import RegularAnalyz

def gen_2d_list():
    """
    ['hf_fantasy', '00:16:44','2019-10-07','2019-10-07']
    """
    r = [['live_traffic', '00:20:17', '2019-08-29 08:50:00', '2019-08-29 09:10:17', '打车 下大雨'], ['live_traffic', '00:15:38', '2019-08-29 08:34:22', '2019-08-29 08:50:00', '大堂等车'], ['live_affair', '00:14:32', '2019-08-29 08:19:49', '2019-08-29 08:34:21', ''], ['sleep', '07:33:56', '2019-08-29 00:45:52', '2019-08-29 08:19:48', ''], ['input_others', '00:05:44', '2019-08-29 00:40:07', '2019-08-29 00:45:51', '看时间记录api相关内容 101camp'], ['live_affair', '00:17:22', '2019-08-29 00:22:45', '2019-08-29 00:40:07', '洗澡等'], ['o_others', '00:09:03', '2019-08-29 00:13:41', '2019-08-29 00:22:44', '看手机'], ['hf_fantasy', '00:12:37', '2019-08-29 00:01:03', '2019-08-29 00:13:40', ''], ['live_affair', '00:05:58', '2019-08-28 23:55:04', '2019-08-29 00:01:02', '预定明天酒店'], ['p_Errands', '11:43:09', '2019-08-28 21:06:51', '2019-08-29 08:50:00', 'green  hotel']]
    return r

def main():
    #------------------------
    #使用本文件中的数据
    j = gen_2d_list()
    #------------------------
    #从csv中获取数据
    #get_data = RegularAnalyz()
    #origin_data_path = "/Users/wangjunyu/101camp/trao/data/r.csv"
    #tmp_file_path = os.path.join(project_root,'data/tmp.trao')
    #j = get_data.fetch_csv(origin_data_path,tmp_file_path)
    print(j)

    #type_list = ['begin']
    type_list = ['live_traffic']

    print(RegularAnalyz.check_activity_type(j,type_list))

if __name__ == "__main__":
    main()
