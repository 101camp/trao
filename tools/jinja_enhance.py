import os
import sys
project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)

from logger import logger
from config import configer

class JinjaEnhance():
    
    def __init__(self):
        pass

    #TODO: 把参数改为可变参数，如果参数不够，返回原值。如果参数够，则正常处理
    @staticmethod
    def combine_data(a,b):
        """
        判断格式并相加
        睡眠总时间|
        40:11
        0:25
        2411mins
        25mins
        """
        import re
        r = re.compile('(0|([1-9]\d{0,})):\d\d')
        x = re.compile('(0|([1-9]\d{0,}))mins')
        if (r.match(a) is not None) and (r.match(b) is not None):
            aa = [int(s) for s in a.split(":") if s.isdigit()]
            bb = [int(s) for s in b.split(":") if s.isdigit()]
            tmp0 = aa[0] + bb[0]
            tmp = aa[1] + bb[1]
            #print(tmp0,tmp)
            if tmp >= 60:
                xxx,yyy = divmod(tmp,60)
                result = str(tmp0+xxx) + ':' + str(yyy)
                return result
            else:
                return str(tmp0) + ':' + str(tmp)
        elif x.match(a) is not None and x.match(b) is not None:
            result = int(b[:-4]) + int(a[:-4])
            print(result)
            return str(result)+ 'mins'

def main():
    pass

if __name__ == "__main__":
    main()
