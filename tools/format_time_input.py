"""
File: format_time_input.py
Description: 
"""

import os
import sys
import arrow
from dateutil import tz

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)

from logger import logger
from config import configer

class TimeProcess():
    def __init__(self):
        pass

    @staticmethod
    def get_time_by_weeknumber(week_num,weekday):
        '''
        通过周数和星期获得指定日期
        '''
        import datetime
        # Warning: 这里的week_num 是从0开始，和iso的从1开始有区别
        d = str(arrow.now('local').year) + "-W" + str(week_num - 1)
        if 0 < weekday < 7:
            wd = weekday
        elif weekday == 7 or weekday == 0:
            wd = 0 
        r = datetime.datetime.strptime(d + '-' + str(wd), "%Y-W%W-%w")
        return arrow.get(r).replace(tzinfo='local')

    @classmethod
    def get_week(cls,input_str,_flag_num):
        """
        input_str: 可以为周数，可以为某一个具体日期等arrow能够接受的数值
        _flag_num: 0:取周一至周五，1：取上周日至本周五
        return: 返回列表，包含开始时间和结束时间;如果输入有误则返回空
        """
        import datetime
        try:
            if 1 <= int(input_str) <= 52:
                d = str(arrow.now('local').year) + "-W" + str(input_str)
                r = datetime.datetime.strptime(d + '-5', "%Y-W%W-%w")
                if _flag_num == 0:
                    start = arrow.get(r ,'local').shift(days=-4)
                    end = arrow.get(r,'local').ceil('day')
                    return [start,end]
                elif _flag_num == 1:
                    start = arrow.get(r ,'local').shift(days=-6)
                    end = arrow.get(r,'local').ceil('day')
                    return [start,end]
                else:
                    print('Wrong input of _flag_num!')
                    return None
        except:
            print('in except')
            #raise
            _r = cls.str_to_time(input_str)
            _week_number = _r.isoweekday()
            # TODO: 代码需要重构，简化
            if _flag_num == 0 and _week_number>5:
                shift_d = _r.isoweekday()-1
                start = _r.shift(days=-shift_d)
                end = start.shift(days=+4).ceil('day')
                return [start,end]
            elif _flag_num == 0 and _week_number<=5:
                shift_d = _r.isoweekday()+6
                start = _r.shift(days=-shift_d)
                end = start.shift(days=+4).ceil('day')
                return [start,end]
            elif _flag_num == 1 and _week_number>5:
                shift_d = _r.isoweekday()+1
                start = _r.shift(days=-shift_d)
                end = start.shift(days=+6).ceil('day')
                return [start,end]
            elif _flag_num == 1 and _week_number <= 5:
                shift_d = _r.isoweekday()+8
                start = _r.shift(days=-shift_d)
                end = start.shift(days=+6).ceil('day')
                return [start,end]
            else:
                print('Wrong input of _flag_num!')
                return None


    #@staticmethod
    @classmethod
    def get_month(cls,input_str):
        """
        input_str: 可是是月份数字，也可以是一个具体的日期
        return: 返回列表，包含开始时间和结束时间;如果输入有误则返回空
        """
        try:
            print(int(input_str))
            if 1<= int(input_str) <=12:
                s = arrow.now('local').replace(month=int(input_str),day=1).span('month')
                return [s[0],s[1]]
            else:
                print("Int but not month")
                return None
        except:
            print("not int,in except")
            s = cls.str_to_time(input_str).replace(day=1,tzinfo=tz.tzlocal()).span('month')
            return [s[0],s[1]]
        else:
            # 考虑到可能和while合用，如果都不对，就返回空，继续While
            return None

    @classmethod
    def gen_start_end_time(cls,*args):
        """
        TODO: Docstring for .
        用于生成开始、结束的具体时间
        *args: arrow可以解析的字符串格式
        return: 返回列表，包含开始时间和结束时间;如果输入有误则返回空
        """
        #print(type(*input_))
        #print(type(input_))
#        if len(args) == 1:
#        # 单个输入的情况
#            print("single arg value:",args)
#            start = cls.str_to_time(args[0])
#            end = start.shift(hours=+23,minutes=+60,seconds=-1)
#        else:
            #print("args value:",args)
        start = cls.str_to_time(args[0])
        end = start.shift(hours=+23,minutes=+60,seconds=-1)
        for arg in args:
            _tmp = cls.str_to_time(arg)
            if _tmp < start:
                start = _tmp
            elif _tmp > start:
                end = _tmp.shift(hours=+23,minutes=+60,seconds=-1)
        return [start,end]

    @staticmethod
    def str_to_time(input_str):
        """
        把输入的字符串转换为时间
        考虑到可能和while合用，如果都不对，就返回空，继续While
        """
        try:
            # TODO: 对应位置的时区信息都要改为这里的写法，考虑到全球不同地区的兼容性
            return arrow.get(input_str,tzinfo=tz.tzlocal())
        except:
            logger.info("Wrong Format: {}".format(input_str))
            return None

    @staticmethod
    def time_range_judge(st,et,s,e):
        '''根据提供的开始时间、结束时间计算是否在某一个区间内。
        st: 开始时间
        et: 结束时间
        s: 第一个开始时间
        e: 第一个结束时间
        return: True or False
        '''
        in_ = st <= s <= et and st <= e <= et
        return in_
        




def main():
    month_input_str = input("Plean input your month:")
    #print(TimeProcess.get_month(month_input_str))
    #print(TimeProcess.str_to_time(month_input_str))
    print(TimeProcess.get_week(month_input_str,0))
    print(TimeProcess.get_week(month_input_str,1))

    # 测试gen_start_end_time
    #input_v = month_input_str
    #input_v = ['2019-10-04','2019年10月04日']
    input_v = "2019-10-04"
    #print("input_v type:",type(input_v))
    #tmp = TimeProcess.gen_start_end_time(input_v)
    #tmp = TimeProcess.gen_start_end_time("2019-10-04")
    #tmp = TimeProcess.gen_start_end_time()
    #tmp = TimeProcess.gen_start_end_time('2019-10-04','2019-10-05','2019-10-03')
    #print(tmp)


if __name__ == "__main__":
    main()
