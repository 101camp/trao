"""
File: data_source.py
Author: John Yul
Description: 对csv文件的读入、检查、存储等
"""

import os
import sys
import json
import csv

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
tmp_file_path = os.path.join(project_root,'data/tmp.trao')
sys.path.append(project_root)
#print(sys.path)
from logger import logger
from config import configer,type_configer

class CsvData():
    def __init__(self):
        pass

    @staticmethod
    #def get_csv_data(self,file_path):
    def get_csv_data(file_path):
        print("file_path:",file_path)
        with open(file_path, 'r') as f:
            reader = csv.reader(f)
            your_list = list(reader)
            #logger.debug(your_list)
            return your_list

    @staticmethod
    def csv_file_check(source_data):
        """
        检查逻辑：
        每一行的数据必须是5个
        类型列的数据会辅助生成对应的类型
        开始时间列和结束时间列都为时间
        结束时间晚于开始时间

        该检查函数可以以后再写，目前得到的数据都比较规范

        """
        #_data_iter = (_i for _i in source_data if len(_i) == 5)
        _data = [_i for _i in source_data if len(_i) == 5]
        _data = [_i for _i in _data if _i != ['','','','','']]
        return _data
        
    @staticmethod 
    def csv_save(list_data,tmp_file_path):
        # TODO: 这里的out.trao 是临时的，要改掉
        # 对list数据的处理
        with open(tmp_file_path,"w", newline="") as f:
            writer = csv.writer(f)
            writer.writerows(list_data)

    @staticmethod
    def csv_save_dict(dict_data,tmp_file_path,mode):
        '''
        Store dict data into csv
        mode: a = add w = rewrite
        '''
        with open(tmp_file_path,mode, newline="") as f:
            w = csv.DictWriter(f, dict_data.keys())
            w.writeheader()
            w.writerow(dict_data)


if __name__ == "__main__":
    data = CsvData()
    name=os.path.join(project_root,'data/csv/report-24-09-2019.csv')
    #tmp = data.get_csv_data(name,tmp_file_path)
    tmp = data.get_csv_data(name)
    r = data.csv_file_check(tmp)
    data.csv_save(r,tmp_file_path)

