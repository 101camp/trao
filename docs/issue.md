# Issue 使用说明

## Issue 的使用

Issue 主要用于对项目相关内容的讨论、提问等。请勿在Issue中灌水，发表不当言论等。

## Issue 的管理

不定期对Issue的内容做排查，将对应的想法转化为开发的来源。对于不合事宜的Issue，管理员会主动关闭。

## Issue 的设置

### Labels

Issue的标签主要分类两类，Issue讨论的话题类型和Issue的内容被考虑的优先级。

> Priority:High
> Priority:Low
> Priority:Medium
> Type:Bug
> Type:Docs
> Type:Enhancement
> Type:Feature
> Type:Idea
> Type:Refactor

该类型会根据项目的情况适当调整

官方文档参考 [Labels](https://gitlab.com/help/user/project/labels.md)

### Template

还在制作中...
官方文档参考:[Description templates](https://gitlab.com/help/user/project/description_templates)
