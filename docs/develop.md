# 开发说明

## 准备工作

请先按照[部署文档](develop.md)的说明配置好项目、安装好依赖等，确保项目可以正常运行。

## 获取需求

* [README](README.md)的功能设计章节 或者 [待开发事项](todo.md)中获取需求
* 也可以直接从Issue中了解正在开发的功能

## 进入开发循环

该项目的开发过程管理通过 Issue 驱动。

*查看/新建 Issue*
* 如果你想开发的功能还没有Issue，请先新建Issue。
* 如果你想开发的功能已经有Issue，请在Issue下留言，商讨合作开发事宜

关于Issue的使用参考[Issue.md](issue.md)

*基于 Issue 新建分支*

* 请按照分支命名要求命名分支，并在自己的开发分支上完成开发。
![2019-09-16-develop-cycle.png (1844×1158)](https://pic.junyuio.com/img/trao/2019-09-16-develop-cycle.png)

*完成单元测试*

*提交 Merge Request*

## 开发规范

### 日志管理
提交代码时不使用print做输出，使用[logging](https://docs.python.org/3/library/logging.html)模块，目前程序已经做好配置。
```
from logger import logger
```
在需要`print()`的时候，输入`logger.info()`，另外注意logger默认的状态中输出为info及以上的内容。
如果需要调整输出的内容，请在logging的配置文件中修改对应的级别即可。


## 分支和Commit管理

*分支管理*
* master 为主分支, 一般情况下仅在release时更新。
* develop 为开发分支, 功能代码需要提交merge request。

* 开发前请新建分支，命名为[feature/detail_descripe],在自己的分支中完成了相关的开发后，再申请merge
> 举例：feature/pyechart 该分支在开发和echart相关的功能

*commit管理*

* 分支提交时的commit撰写规范
	* 参考 [文档](http://karma-runner.github.io/4.0/dev/git-commit-msg.html)
	* 项目中的提交commit分类说明
> * feature：新功能（feature）
> * fix：修补bug
> * docs：文档（documentation）
> * style： 格式（不影响代码运行的变动）
> * refactor：重构（即不是新增功能，也不是修改bug的代码变动）
> * test：增加或者修改测试
> * other：其他未尽事项

举例:
具体的历史提交情况可以参考[这里](https://gitlab.com/101camp/trao/commits/develop)
```
[docs]add todo in code file
[bugfix]type file read error fix
[refactor][wip]change config.py and logger.py    
[others]add logo url
```

注意事项
* commit 提交时确保功能的完备性
* 如果提交时功能不完备会引发报错，需要在提交时附上[wip]说明，上面的例子中有涉及到，这里再次贴出
```
[refactor][wip]change config.py and logger.py    
```
