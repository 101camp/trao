# 部署说明


## Csv 版本

### 配置文件说明
项目正常启动必须要配置如下的配置文件
```
config
├── config.ini
└── logging_config.ini
```
这些文件在`config`文件夹中有对应的样例文件，把样例文件的`.example`后缀去掉，参考下方说明填写对应的内容即可。
```
config
├── config.ini.example
├── logging_config.ini.example
└── type.ini.example
```

### config.ini
项目的主配置文件，填写效果如下
```
[path_config]
weekly_jinja_template = data/templates/jinja_template.j2
weekly_save_name = data/result.md

[tr_related]
days_new = 1
```
* path_config 中提到的j2后缀的文件为jinja的输出模板，需要配置，下文会详细说明
* 注意，在等号后面直接写值，不需要双引号

### logging_config.ini
* 该配置文件主要配置开发中应用的日志管理模块，内容已经配置完成，功能可用。
* 如果希望调整日志输出的层级，只需要将下面的代码中`level=DEBUG`修改为`level=INFO`或者其他等级即可。
```
[logger_trao]
level=DEBUG
handlers=stream_handler
qualname=trao
propagate=0
```

### Jinja模板配置

在项目`data/templates`目录下还有`jinja_template.j2.example`文件，去掉`.example`后缀。
文件中是用户希望输出的内容模板格式，需要根据Jinja2的模板配置方式做进一步的配置。

在程序的输出中，会展示最终结果的字典数据，类似如下结构
```
{'live_dinner_type_name': 'live_dinner', 'live_dinner_all_m': '310mins', 'live_dinner_all_m_h': '5:10', 'live_dinner_times': 17, 'live_dinner_everage': '44mins', 'live_dinner_everage_h': '0:44', 'live_dinner_detail_list': [['live_dinner', '00:16:17', '2019-10-23 17:30:53 +08:00', '2019-10-23 17:47:10 +08:00', '火车上', datetime.timedelta(seconds=977)], ['live_dinner', '00:13:01', '2019-10-23 17:17:51 +08:00', '2019-10-23 17:30:52 +08:00', '买饭', datetime.timedelta(seconds=781)], ['live_dinner', '00:09:37', '2019-10-23 12:23:15 +08:00', '2019-10-23 12:32:52 +08:00', '吃肉夹馍', datetime.timedelta(seconds=577)], ['live_dinner', '00:24:44', '2019-10-23 11:58:30 +08:00', '2019-10-23 12:23:14 +08:00', '西少爷', datetime.timedelta(seconds=1484)], 'start_date': '2019-10-17', 'end_date': '2019-10-23', 'number_of_week': 43}
```

可以把字典的key作为Jinja模板中的变量，解析后会展示对应的value。建议基于上面的输出数据为参考，结合[data/templates/jinja_template.j2.example](data/templates/jinja_template.j2.example)文件理解配置。


### 依赖安装

在项目根目录下，执行`pip install -r requirements.txt`安装所有需要的依赖。如果部分内容安装太慢，可以使用国内镜像安装，执行`pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple`。

## 项目启动方法

### 提供csv数据文件

请用atimelogger导出csv文件，并把文件放置在data目录下，修改文件名为`r.csv`。

### 启动项目

项目根目录下执行`python driver.py`
可以利用`python driver.py -h`获取具体的参数含义和实例。

### 输出内容说明

* 在终端输出一个巨大的字典，该字典中的Key可以用于配置Jinja2的模板
* data/log 目录下对应时间的log内容，和终端输出的内容一致
* data目录下生成的result.md
* 系统剪贴板中的内容，和result.md的内容一致

## Api 版本(暂停使用)

由于atl2的api发生变更，目前该功能暂停使用，等完成api调整后再启用。

由于时间记录涉及到账号密码、个人数据隐私等内容。所以项目仅提供了部分example的文件，为了让项目正常运行，需要把example文件中的内容替换。并且目前项目设置好了.gitignore文件，所以替换后的文件正常情况下不会上传到项目中，请放心使用。

### 配置文件说明
项目正常启动必须要配置如下的配置文件
```
config
├── config.ini
├── logging_config.ini
└── type.ini
```
这些文件在`config`文件夹中有对应的样例文件，把样例文件的`.example`后缀去掉，参考下方说明填写对应的内容即可。
```
config
├── config.ini.example
├── logging_config.ini.example
└── type.ini.example
```

### config.ini
项目的主配置文件，填写效果如下
```
[atl2_account_info]
username = your_email
password = your_password

[path_config]
weekly_jinja_template = data/templates/jinja_template.j2
weekly_save_name = data/result.md

[tr_related]
interval_max = 100000
days_new = 1
```
* atl2的账号密码为必填项
* path_config 中提到的j2后缀的文件为jinja的输出模板，需要配置，下文会详细说明
* 注意，在等号后面直接写值，不需要双引号

### logging_config.ini
* 该配置文件主要配置开发中应用的日志管理模块，内容已经配置完成，功能可用。
* 如果希望调整日志输出的层级，只需要将下面的代码中`level=DEBUG`修改为`level=INFO`或者其他等级即可。
```
[logger_trao]
level=DEBUG
handlers=stream_handler
qualname=trao
propagate=0
```

### type.ini
* 存储从api获取的所有type和group的属性
* 执行方法
  * 先完成对 config.ini 的配置
  * 完成下方的依赖安装工作
  * 过执行`python type_init.py` 初始化该文件

## Jinja模板配置

在项目`data/templates`目录下还有`jinja_template.j2.example`文件，去掉`.example`后缀。
文件中是用户希望输出的内容模板格式，需要根据Jinja2的模板配置方式做进一步的配置。

## 依赖安装

在项目根目录下，执行`pip install -r requirements.txt`安装所有需要的依赖。如果部分内容安装太慢，可以使用国内镜像安装，执行`pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple`。

## 项目启动方法

### 启动项目
在项目根目录下执行 `python src/weekly_new.py`

### 输出内容说明

* 在终端输出一个巨大的字典，该字典中的Key可以用于配置Jinja2的模板
* data/log 目录下对应时间的log内容，和终端输出的内容一致
* data目录下生成的result.md
* 系统剪贴板中的内容，和result.md的内容一致
